#!/bin/sh
#
#    liberate_icons.sh
#    Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
#                                            <neva_blyad@lovecri.es>
#
#    This file is part of black_white-libre.
#
#    black_white-libre is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    black_white-libre is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with black_white-libre.  If not, see <https://www.gnu.org/licenses/>.

MAX_PROCS=$((`nproc` + 1))

################################################################################
# Make libre icons acid
################################################################################

VECTOR_HEADER_LIBRE='filter="url(#acid)"'
VECTOR_FOOTER_LIBRE='<filter id="acid">\
    <feColorMatrix\
        type="matrix"\
        values="0.68627451  0  0  0  0\
                0           1  0  0  0\
                0           0  0  0  0\
                0           0  0  1  0 "/>\
</filter>\
'

RASTER_LIBRE="'0.68627451  0  0\
               0           1  0\
               0           0  0'"

find $1 -type f \(                            \
                   -iname "*.svg"             \
                \)                            \
                \(                            \
                   -iname "*libre*"       -or \
                   -iname "*liberation*"  -or \
                   -iname "*liberty*"     -or \
                   -iname "*libertine*"   -or \
                                              \
                   -iname "*firefox*"     -or \
                   -iname "*thunderbird*"     \
                \) |                                                               \
    xargs -I% -P$MAX_PROCS sh -c "echo %;                                          \
                                  sed -i -e 's|<svg|<svg $VECTOR_HEADER_LIBRE|' %; \
                                  sed -i -e 's|</svg>|$VECTOR_FOOTER_LIBRE</svg>|' %"

find $1 -type f \(                            \
                   -iname "*.png"         -or \
                   -iname "*.xpm"             \
                \)                            \
                \(                            \
                   -iname "*libre*"       -or \
                   -iname "*liberation*"  -or \
                   -iname "*liberty*"     -or \
                   -iname "*libertine*"   -or \
                                              \
                   -iname "*firefox*"     -or \
                   -iname "*thunderbird*"     \
                \) |                         \
    xargs -I% -P$MAX_PROCS sh -c "echo %;    \
                                  convert % -recolor $RASTER_LIBRE %"

################################################################################
# Make other icons grayscale
################################################################################

#VECTOR_HEADER='filter="url(#desaturate)"'
#VECTOR_FOOTER='<filter id="desaturate">\
#    <feColorMatrix type="matrix" values="0.3333  0.3333  0.3333  0  0\
#                                         0.3333  0.3333  0.3333  0  0\
#                                         0.3333  0.3333  0.3333  0  0\
#                                         0       0       0       1  0"/>\
#</filter>\
#'
#RASTER="'.3333   .3333   .3333\
#         .3333   .3333   .3333\
#         .3333   .3333   .3333'"

#VECTOR_HEADER='filter="url(#gray-on-light)"'
#VECTOR_FOOTER='<filter id="gray-on-light">\
#  <feColorMatrix\
#    type="matrix"\
#    values="1   0   0   0   0\
#            1   0   0   0   0\
#            1   0   0   0   0\
#            0   0   0   1   0 "/>\
#</filter>\
#'\
#
#RASTER="'1  0  0\
#         1  0  0\
#         1  0  0'"

VECTOR_HEADER='filter="url(#gray-on-mid)"'
VECTOR_FOOTER='<filter id="gray-on-mid">\
  <feColorMatrix\
    type="matrix"\
    values="0  1  0  0  0\
            0  1  0  0  0\
            0  1  0  0  0\
            0  0  0  1  0"/>\
</filter>\
'\

RASTER="'0  1  0\
         0  1  0\
         0  1  0'"

#VECTOR_HEADER='filter="url(#gray-on-dark)"'
#VECTOR_FOOTER='<filter id="gray-on-dark">\
#  <feColorMatrix\
#    type="matrix"\
#    values="0  0  1  0  0\
#            0  0  1  0  0\
#            0  0  1  0  0\
#            0  0  0  1  0"/>\
#</filter>\
#'
#
#RASTER="'0  0  1\
#         0  0  1\
#         0  0  1'"

find $1 -type f \(                                                           \
                   -iname "*.svg"                                            \
                \)                                                           \
                -not                                                         \
                \(                                                           \
                   -iname "*libre*"       -or                                \
                   -iname "*liberation*"  -or                                \
                   -iname "*liberty*"     -or                                \
                   -iname "*libertine*"   -or                                \
                                                                             \
                   -iname "*firefox*"     -or                                \
                   -iname "*thunderbird*"                                    \
                \) |                                                         \
    xargs -I% -P$MAX_PROCS sh -c "echo %;                                    \
                                  sed -i -e 's|<svg|<svg $VECTOR_HEADER|' %; \
                                  sed -i -e 's|</svg>|$VECTOR_FOOTER</svg>|' %"

find $1 -type f \(                            \
                   -iname "*.png"         -or \
                   -iname "*.xpm"             \
                \)                            \
                -not                          \
                \(                            \
                   -iname "*libre*"       -or \
                   -iname "*liberation*"  -or \
                   -iname "*liberty*"     -or \
                   -iname "*libertine*"   -or \
                                              \
                   -iname "*firefox*"     -or \
                   -iname "*thunderbird*"     \
                \) |                         \
    xargs -I% -P$MAX_PROCS sh -c "echo %;    \
                                  convert % -recolor $RASTER %"
