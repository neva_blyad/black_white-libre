#    Makefile
#    Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
#                                            <neva_blyad@lovecri.es>
#
#    This file is part of black_white-libre.
#
#    black_white-libre is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    black_white-libre is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with black_white-libre.  If not, see <https://www.gnu.org/licenses/>.

.ONESHELL:
.PHONY: all general laptop desktop install clean deinstall

# Source path

BIN   := $(CURDIR)/bin/
BUILD := $(CURDIR)/build/
DL    := $(CURDIR)/dl/
SRC   := $(CURDIR)/src/

BUILD_APPLICATIONS := $(BUILD)/share/applications/
BUILD_ICONS        := $(BUILD)/share/icons/
BUILD_THEMES       := $(BUILD)/share/themes/

SRC_APPLICATIONS   := $(SRC)/share/applications/
SRC_ICONS          := $(SRC)/share/icons/
SRC_THEMES         := $(SRC)/share/themes/

# Destination path

PREFIX ?= $(HOME)/.local/

PREFIX_APPLICATIONS := $(PREFIX)/share/applications/
PREFIX_ICONS        := $(PREFIX)/share/icons/
PREFIX_THEMES       := $(PREFIX)/share/themes/

all: laptop

general:
	
	################################################################################
	# Applications
	################################################################################
	
	mkdir -p '$(BUILD_APPLICATIONS)'
	cd '$(BUILD_APPLICATIONS)'
	
	# *.desktop
	cp /usr/share/applications/calibre-lrfviewer.desktop ./
	sed -i -e 's|LRF viewer|calibre LRF Viewer|' calibre-lrfviewer.desktop
	cp /usr/share/applications/calibre-ebook-edit.desktop ./
	sed -i -e 's|E-book editor|calibre E-book Editor|' calibre-ebook-edit.desktop
	cp /usr/share/applications/calibre-ebook-viewer.desktop ./
	sed -i -e 's|E-book viewer|calibre E-book Viewer|' calibre-ebook-viewer.desktop
	cp /usr/share/applications/menulibre.desktop ./
	sed -i -e '\|^Name\[|d' menulibre.desktop
	sed -i -e 's|Menu Editor|MenuLibre|' menulibre.desktop
	cp /usr/share/applications/org.gnome.GPaste.Ui.desktop ./
	sed -i -e 's|edit-paste|xclipboard|' org.gnome.GPaste.Ui.desktop
	cp /usr/share/applications/qtconfig-qt4.desktop ./
	sed -i -e 's|Icon=qtconfig-qt4|Icon=preferences-desktop-theme|' qtconfig-qt4.desktop
	cp /usr/share/applications/system-config-lvm.desktop ./
	sed -i -e 's|/usr/share/system-config-lvm/pixmaps/lv_icon.png|lv_icon|' system-config-lvm.desktop
	cd '$(BUILD_APPLICATIONS)'
	
	################################################################################
	# Icons
	################################################################################
	
	mkdir -p '$(BUILD_ICONS)'
	cd '$(BUILD_ICONS)'
	
	# Adwaita
	cp -r /usr/share/icons/Adwaita/ ./
	cd Adwaita/
	'$(BIN)/liberate_icons.sh' ./
	gtk-update-icon-cache ./
	gtk4-update-icon-cache -f ./
	cd '$(BUILD_ICONS)'
	
	# black_white-libre
	unzip '$(DL)/black-white-icon-theme-2-es-en-br-fr-de-it-cn-jp-ar-ru-ubu.zip'
	tar xzf 'black-white 2 Style/black-white_2-Style.tar.gz'
	rm -r 'black-white 2 Style/'
	cp -r '$(SRC_ICONS)/black_white-libre/' ./
	cd black_white-libre/
	mkdir -p 16x16/places/
	cp -r ../Adwaita/16x16/places/ ./16x16/
	mkdir -p 22x22/places/
	cp -r ../Adwaita/22x22/places/ ./22x22/
	mkdir -p 24x24/places/
	cp -r ../Adwaita/24x24/places/ ./24x24/
	mkdir -p 32x32/places/
	cp -r ../Adwaita/32x32/places/ ./32x32/
	mkdir -p 48x48/places/
	cp -r ../Adwaita/48x48/places/ ./48x48/
	mkdir -p 64x64/places/
	cp -r ../Adwaita/64x64/places/ ./64x64/
	mkdir -p 96x96/places/
	cp -r ../Adwaita/96x96/places/ ./96x96/
	mkdir -p 256x256/places/
	cp -r ../Adwaita/256x256/places/ ./256x256/
	mkdir -p 512x512/places/
	cp -r ../Adwaita/512x512/places/ ./512x512/
	cd scalable/
	cd apps/
	cd 256/
	cp '$(BUILD_ICONS)/black-white_2-Style/scalable/devices/256/drive-harddisk.png' lv_icon.png
	cd ../
	cp '$(BUILD_ICONS)/black-white_2-Style/scalable/apps/f-spot.png' geeqie.png
	'$(BIN)/liberate_icons.sh' geeqie.png
	cp '$(BUILD_ICONS)/black-white_2-Style/scalable/apps/rhythmbox.png' org.gnome.Rhythmbox.png
	cp '$(BUILD_ICONS)/black-white_2-Style/scalable/apps/synaptic.png' ./
	cp '$(BUILD_ICONS)/black-white_2-Style/scalable/apps/virtualbox.png' ./
	cp '$(BUILD_ICONS)/black-white_2-Style/scalable/apps/xclipboard.png' ./
	cp /usr/share/pixmaps/anbox.png ./
	'$(BIN)/liberate_icons.sh' anbox.png
	cp /usr/share/pixmaps/assistant-qt5.png ./
	'$(BIN)/liberate_icons.sh' assistant-qt5.png
	cp /usr/share/pixmaps/designer-qt5.png ./
	'$(BIN)/liberate_icons.sh' designer-qt5.png
	cp /usr/share/pixmaps/htop.png ./
	'$(BIN)/liberate_icons.sh' htop.png
	cp /usr/share/pixmaps/links2.xpm ./
	'$(BIN)/liberate_icons.sh' links2.xpm
	cp /usr/share/pixmaps/net329edit.png ./
	'$(BIN)/liberate_icons.sh' net329edit.png
	cp /usr/share/pixmaps/linguist-qt5.png ./
	'$(BIN)/liberate_icons.sh' linguist-qt5.png
	cp /usr/share/pixmaps/sqlitebrowser.svg ./
	'$(BIN)/liberate_icons.sh' sqlitebrowser.svg
	cp ~/.local/share/icons/telegram.png ./
	chmod u+w telegram.png
	'$(BIN)/liberate_icons.sh' telegram.png
	cd ../
	cd devices/
	cd 256/
	cp '$(BUILD_ICONS)/black-white_2-Style/scalable/devices/256/computer.png' ./
	cd ../
	cp '$(BUILD_ICONS)/black-white_2-Style/scalable/devices/stock_notebook.png' ./
	cd ../
	cp -r '$(BUILD_ICONS)/black-white_2-Style/scalable/places/' ./
	cd places/
	cd 256/
	mv folder-downloads.png folder-download.png
	mv folder-Images.png folder-pictures.png
	mv folder-movies.png folder-videos.png
	gtk-update-icon-cache ./
	gtk4-update-icon-cache -f ./
	cd '$(BUILD_ICONS)'
	
	#unzip ../src/black-white-icon-theme-2-es-en-br-fr-de-it-cn-jp-ar-ru-ubu.zip
	#tar xzf 'black-white 2 Style/black-white_2-Style.tar.gz'
	#rm -r 'black-white 2 Style/'
	#cd black-white_2-Style/scalable/
	#cd apps/
	#cd 256/
	#cp ../../devices/256/drive-harddisk.png gnome-disks.png
	#ln -s accessories-text-editor.png gedit.png
	#ln -s logviewer.png gnome-logs.png
	#ln -s totem.png org.gnome.Totem.png
	#cd ../
	#cp ../mimetypes/stock_addressbook.png org.gnome.Contacts.png
	#ln -s clock.png org.gnome.clocks.png
	#ln -s dates.png org.gnome.Calendar.png
	#ln -s document-properties.png org.gnome.tweaks.png
	#cp f-spot.png geeqie.png
	#ln -s gconf-editor.png dconf-editor.png
	#ln -s gnome-character-map.png gnome-characters.png
	#ln -s gnome-grecord.png gnome-sound-recorder.png
	#ln -s gnome-monitor.png gnome-system-monitor.png
	#ln -s seahorse-preferences.png seahorse.png
	#cd ../
	#cd devices/
	#rm xsane.png
	#cd ../
	#cd places/
	#cd 256/
	#ln -s folder-downloads.png folder-download.png
	#ln -s folder-movies.png folder-videos.png
	#ln -s folder-Images.png folder-pictures.png
	#cd '$(BUILD_ICONS)'
	#mv black-white_2-Style/ black-white_3/
	
	# gnome
	cp -r /usr/share/icons/gnome/ ./
	cd gnome/
	'$(BIN)/liberate_icons.sh' ./
	gtk-update-icon-cache ./
	gtk4-update-icon-cache -f ./
	cd '$(BUILD_ICONS)'
	
	# hicolor
	cp -r /usr/share/icons/hicolor/ ./
	cd hicolor/
	cp --remove-destination /usr/share/firefox/browser/chrome/icons/default/default128.png 128x128/apps/firefox.png
	cp --remove-destination /usr/share/firefox/browser/chrome/icons/default/default16.png 16x16/apps/firefox.png
	cp --remove-destination /usr/share/firefox/browser/chrome/icons/default/default32.png 32x32/apps/firefox.png
	cp --remove-destination /usr/share/firefox/browser/chrome/icons/default/default48.png 48x48/apps/firefox.png
	cp --remove-destination /usr/share/firefox/browser/chrome/icons/default/default64.png 64x64/apps/firefox.png
	cp --remove-destination /usr/share/firefox-esr/browser/chrome/icons/default/default128.png 128x128/apps/firefox-esr.png
	cp --remove-destination /usr/share/firefox-esr/browser/chrome/icons/default/default16.png 16x16/apps/firefox-esr.png
	cp --remove-destination /usr/share/firefox-esr/browser/chrome/icons/default/default32.png 32x32/apps/firefox-esr.png
	cp --remove-destination /usr/share/firefox-esr/browser/chrome/icons/default/default48.png 48x48/apps/firefox-esr.png
	cp --remove-destination /usr/share/firefox-esr/browser/chrome/icons/default/default64.png 64x64/apps/firefox-esr.png
	cp --remove-destination /usr/share/games/supertux2/images/engine/icons/supertux-256x256.png 256x256/apps/supertux.png
	cp --remove-destination /usr/share/pixmaps/tortoisehg/icons/scalable/apps/thg.svg scalable/apps/thg_logo.svg
	'$(BIN)/liberate_icons.sh' ./
	gtk-update-icon-cache ./
	gtk4-update-icon-cache -f ./
	cd '$(BUILD_ICONS)'
	
	# locolor
	cp -r /usr/share/icons/locolor/ ./
	cd locolor/
	'$(BIN)/liberate_icons.sh' ./
	gtk-update-icon-cache ./
	gtk4-update-icon-cache -f ./
	cd '$(BUILD_ICONS)'
	
	################################################################################
	# Theme
	################################################################################
	
	mkdir -p '$(BUILD_THEMES)'
	cd '$(BUILD_THEMES)'
	
	# black_white-libre
	cp -r '$(SRC_THEMES)/black_white-libre/' ./

desktop: general
	
	################################################################################
	# Icons
	################################################################################
	
	cd '$(BUILD_ICONS)'
	
	# black_white-libre
	cd black_white-libre/
	cd scalable/
	cd apps/
	ln -s ../devices/256/computer.png org.gnome.Nautilus.png
	cd '$(BUILD_ICONS)'
	
	cd '$(CURDIR)'
	touch .desktop

laptop: general
	
	################################################################################
	# Icons
	################################################################################
	
	cd '$(BUILD_ICONS)'
	
	# black_white-libre
	cd black_white-libre/
	cd scalable/
	cd apps/
	ln -s ../devices/stock_notebook.png org.gnome.Nautilus.png
	cd '$(BUILD_ICONS)'
	
	cd '$(CURDIR)'
	touch .laptop

install:
	
	cd '$(BUILD)'
	mkdir -p '$(PREFIX)'
	cp -r * '$(PREFIX)'
	
	gnome-shell-extension-tool -e user-theme@gnome-shell-extensions.gcampax.github.com
	
	dconf write /org/gnome/desktop/interface/document-font-name  "'Liberation Sans Narrow Condensed 11'"
	dconf write /org/gnome/desktop/interface/font-name           "'Liberation Sans Narrow Condensed 11'"
	dconf write /org/gnome/desktop/interface/icon-theme          "'black_white-libre'"
	dconf write /org/gnome/desktop/interface/monospace-font-name "'Liberation Mono 11'"
	dconf write /org/gnome/desktop/interface/text-scaling-factor 1
	
	dconf write /org/gnome/desktop/wm/preferences/titlebar-font  "'Liberation Sans Narrow Condensed 11'"
	
	dconf write /org/gnome/settings-daemon/plugins/xsettings/hinting "'slight'"
	
	cd '$(CURDIR)'
	test -f .desktop && dconf write /org/gnome/settings-daemon/plugins/xsettings/antialiasing "'grayscale'" || \
	                    dconf write /org/gnome/settings-daemon/plugins/xsettings/antialiasing "'rgba'"
	
	dconf write /org/gnome/shell/extensions/user-theme/name "'black_white-libre'"

clean:
	
	rm -rf '$(BUILD)'
	rm .desktop
	rm .laptop

deinstall:
	
	################################################################################
	# Applications
	################################################################################
	
	# *.desktop
	rm '$(PREFIX_APPLICATIONS)/calibre-ebook-viewer.desktop'
	rm '$(PREFIX_APPLICATIONS)/calibre-lrfviewer.desktop'
	rm '$(PREFIX_APPLICATIONS)/calibre-ebook-edit.desktop'
	rm '$(PREFIX_APPLICATIONS)/menulibre.desktop'
	rm '$(PREFIX_APPLICATIONS)/org.gnome.GPaste.Ui.desktop'
	rm '$(PREFIX_APPLICATIONS)/qtconfig-qt4.desktop'
	rm '$(PREFIX_APPLICATIONS)/system-config-lvm.desktop'
	
	################################################################################
	# Icons
	################################################################################
	
	# Adwaita
	rm -r '$(PREFIX_ICONS)/Adwaita/'
	
	# black_white-libre
	rm -r '$(PREFIX_ICONS)/black_white-libre/'
	
	# gnome
	rm -r '$(PREFIX_ICONS)/gnome/'
	
	# hicolor
	rm -r '$(PREFIX_ICONS)/hicolor/'
	
	# locolor
	rm -r '$(PREFIX_ICONS)/locolor/'
	
	################################################################################
	# Theme
	################################################################################
	
	# black_white-libre
	rm -r '$(PREFIX_THEMES)/black_white-libre/'
	
	gnome-shell-extension-tool -d user-theme@gnome-shell-extensions.gcampax.github.com
	
	dconf reset /org/gnome/desktop/interface/document-font-name
	dconf reset /org/gnome/desktop/interface/font-name
	dconf reset /org/gnome/desktop/interface/icon-theme
	dconf reset /org/gnome/desktop/interface/monospace-font-name
	dconf reset /org/gnome/desktop/interface/text-scaling-factor
	dconf reset /org/gnome/desktop/wm/preferences/titlebar-font
	dconf reset /org/gnome/settings-daemon/plugins/xsettings/hinting
	dconf reset /org/gnome/settings-daemon/plugins/xsettings/antialiasing
	dconf reset /org/gnome/shell/extensions/user-theme/name
